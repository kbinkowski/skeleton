package com.kbware.skeleton.module.user


import com.kbware.skeleton.module.user.persistance.UserDocument
import com.kbware.skeleton.module.user.persistance.UserRepository
import org.mapstruct.factory.Mappers
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification
import spock.lang.Subject

import static com.kbware.skeleton.module.user.model.UserRole.ADMIN
import static com.kbware.skeleton.module.user.model.UserRole.SIMPLE
import static com.kbware.skeleton.module.user.model.UserStatus.ACTIVE
import static com.kbware.skeleton.module.user.model.UserStatus.INACTIVE

@DataMongoTest
@ActiveProfiles("test")
class UserServiceIntegrationTest extends Specification {

    @Subject
    UserService userService

    @Autowired
    UserRepository userRepository

    private Pageable pageable = new PageRequest(0, 10, Sort.by("login"))

    def setup() {

        userService = new UserService(
                userRepository,
                Mappers.getMapper(UserMapper.class),
                new com.kbware.skeleton.module.user.persistance.UserPredicateFactory(),
                new BCryptPasswordEncoder()
        )

        userRepository.deleteAll()
        userRepository.saveAll([
                new UserDocument(login: "admin", email: "a@test.pl", status: ACTIVE, roles: [ADMIN]),
                new UserDocument(login: "user1", email: "u1@test.pl", status: ACTIVE, roles: [SIMPLE]),
                new UserDocument(login: "user2", email: "u2@test.pl", status: INACTIVE, roles: [SIMPLE])
        ])

    }


    def "should find all users"() {
        given:
        def criteria = new com.kbware.skeleton.module.user.dto.UserCriteria()

        when:
        Page<com.kbware.skeleton.module.user.model.UserData> users = userService.getUsers(criteria, pageable)

        then:
        users.totalElements == 3
    }

    def "should find by login"() {
        given:
        def criteria = new com.kbware.skeleton.module.user.dto.UserCriteria(term: "user")

        when:
        Page<com.kbware.skeleton.module.user.model.UserData> page = userService.getUsers(criteria, pageable)

        then:
        page.totalElements == 2
        page.collect().login == ["user1", "user2"]
    }

    def "should find by status"() {
        given:
        def criteria = new com.kbware.skeleton.module.user.dto.UserCriteria(status: ACTIVE)

        when:
        Page<com.kbware.skeleton.module.user.model.UserData> page = userService.getUsers(criteria, pageable)

        then:
        page.totalElements == 2
        page.collect().login == ["admin", "user1"]
    }

    def "should find by role"() {
        given:
        def criteria = new com.kbware.skeleton.module.user.dto.UserCriteria(role: ADMIN)

        when:
        Page<com.kbware.skeleton.module.user.model.UserData> page = userService.getUsers(criteria, pageable)

        then:
        page.totalElements == 1
        page.collect().login == ["admin"]
    }

}
