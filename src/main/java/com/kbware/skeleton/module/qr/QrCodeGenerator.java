package com.kbware.skeleton.module.qr;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.qrcode.QRCodeWriter;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.awt.image.BufferedImage;

@RestController
public class QrCodeGenerator {

    @PostMapping(value = "/barcodes", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<BufferedImage> createBarcode(@RequestBody String text) throws WriterException {
        return ResponseEntity.ok(generateQRCodeImage(text));
    }

    public static BufferedImage generateQRCodeImage(String barcodeText) throws WriterException {
        return MatrixToImageWriter.toBufferedImage(
                new QRCodeWriter().encode(barcodeText, BarcodeFormat.QR_CODE, 200, 200)
        );
    }

}
