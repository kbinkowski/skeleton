package com.kbware.skeleton.module.user;

import com.kbware.skeleton.common.select.SelectEntry;
import com.kbware.skeleton.module.user.dto.SaveUserRequest;
import com.kbware.skeleton.module.user.dto.UserCriteria;
import com.kbware.skeleton.module.user.model.UserData;
import com.kbware.skeleton.module.user.model.UserRole;
import com.kbware.skeleton.module.user.model.UserStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/statuses")
    public List<SelectEntry> getStatuses() {
        return SelectEntry.toEntries(UserStatus.values());
    }

    @GetMapping("/roles")
    public List<SelectEntry> getRoles() {
        return SelectEntry.toEntries(UserRole.values());
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public Page<UserData> getUsers(
            UserCriteria criteria,
            @PageableDefault(sort = "createdDate") Pageable pageable
    ) {
        return userService.getUsers(criteria, pageable);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public UserData getUser(@PathVariable String id) {
        return userService.getUser(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public UserData addUser(@PathVariable String id, @RequestBody SaveUserRequest request) {
        return userService.addUser(request);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public UserData updateUser(@PathVariable String id, @RequestBody SaveUserRequest request) {
        return userService.updateUser(id, request);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteUser(@PathVariable String id) {
        userService.deleteUser(id);
    }

}
