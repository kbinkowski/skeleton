package com.kbware.skeleton.module.user.persistance;

import com.kbware.skeleton.common.PredicateFactory;
import com.kbware.skeleton.module.user.dto.UserCriteria;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserPredicateFactory extends PredicateFactory {

    private static final QUserDocument entity = QUserDocument.userDocument;

    public UserPredicateFactory() {
        super(entity.id.isNotNull());
    }

    public BooleanExpression getPredicate(UserCriteria criteria) {
        return List.of(
                createCriteriaEqual(criteria.getStatus(), entity.status),
                createCriteriaContains(criteria.getRole(), entity.roles),
                createTermCriteria(criteria.getTerm(), entity.login, entity.email, entity.firstname, entity.lastname)
        ).stream()
            .filter(expr -> !expr.equals(anyBoolean))
            .reduce(BooleanExpression::and)
            .orElse(anyBoolean);
    }

}
