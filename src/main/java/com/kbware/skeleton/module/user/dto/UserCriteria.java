package com.kbware.skeleton.module.user.dto;

import com.kbware.skeleton.module.user.model.UserRole;
import com.kbware.skeleton.module.user.model.UserStatus;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserCriteria {

    String term;

    UserStatus status;

    UserRole role;

}
