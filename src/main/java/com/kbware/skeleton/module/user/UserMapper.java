package com.kbware.skeleton.module.user;

import com.kbware.skeleton.module.auth.model.AuthenticationUser;
import com.kbware.skeleton.module.user.model.UserData;
import com.kbware.skeleton.module.user.persistance.UserDocument;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    UserData toUserData(UserDocument user);

    AuthenticationUser toAuthenticationUser(UserDocument user);

}
