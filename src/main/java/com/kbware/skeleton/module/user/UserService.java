package com.kbware.skeleton.module.user;

import com.kbware.skeleton.common.exception.NotFoundException;
import com.kbware.skeleton.module.user.dto.SaveUserRequest;
import com.kbware.skeleton.module.user.dto.UserCriteria;
import com.kbware.skeleton.module.user.model.UserData;
import com.kbware.skeleton.module.user.model.UserRole;
import com.kbware.skeleton.module.user.model.UserStatus;
import com.kbware.skeleton.module.user.persistance.UserDocument;
import com.kbware.skeleton.module.user.persistance.UserPredicateFactory;
import com.kbware.skeleton.module.user.persistance.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository repository;

    private final UserMapper mapper;

    private final UserPredicateFactory predicateFactory;

    private final PasswordEncoder passwordEncoder;

    //TODO temporary
    @PostConstruct
    void init() {
        var login = "admin";

        var adminUser = repository.findByLoginAndStatus(login, UserStatus.ACTIVE);
        adminUser.ifPresent(repository::delete);

        repository.save(
                UserDocument.builder()
                        .firstname("ADMIN")
                        .lastname("USER")
                        .login(login)
                        .password(passwordEncoder.encode(login))
                        .email("test@gmail.com")
                        .status(UserStatus.ACTIVE)
                        .roles(List.of(UserRole.ADMIN))
                        .build()
        );
    }

    public Page<UserData> getUsers(UserCriteria criteria, Pageable pageable) {
        return repository.findAll(predicateFactory.getPredicate(criteria), pageable)
                .map(mapper::toUserData);
    }

    public UserData getUser(String id) {
        return mapper.toUserData(getRequiredUser(id));
    }

    public UserData addUser(SaveUserRequest request) {
        return copyAndSave(new UserDocument(), request);
    }

    public UserData updateUser(String id, SaveUserRequest request) {
        return copyAndSave(getRequiredUser(id), request);
    }

    public void deleteUser(String id) {
        repository.delete(getRequiredUser(id));
    }

    private UserData copyAndSave(UserDocument user, SaveUserRequest request) {
        BeanUtils.copyProperties(request, user);
        return mapper.toUserData(repository.save(user));
    }

    private UserDocument getRequiredUser(@NonNull String id) {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("user", id));
    }
}
