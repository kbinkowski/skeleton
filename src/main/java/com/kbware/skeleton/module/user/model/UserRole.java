package com.kbware.skeleton.module.user.model;

import com.kbware.skeleton.common.select.IsSelectable;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum UserRole implements IsSelectable {
    SIMPLE("Użytkownik"),
    ADMIN("Administrator");

    String value;

    @Override
    public String getId() {
        return this.name();
    }
}
