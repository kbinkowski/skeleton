package com.kbware.skeleton.module.user.persistance;

import com.kbware.skeleton.common.audit.AuditEntity;
import com.kbware.skeleton.module.user.model.UserRole;
import com.kbware.skeleton.module.user.model.UserStatus;
import com.querydsl.core.annotations.QueryEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@QueryEntity
@Document("skeleton_user")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDocument extends AuditEntity {

    String firstname;

    String lastname;

    @Indexed(unique = true)
    String login;

    @Indexed(unique = true)
    String email;

    String password;

    @Indexed
    UserStatus status;

    List<UserRole> roles;

    LocalDateTime modifyStatusDate;

    LocalDateTime lastLoginDate;

    public void setStatus(UserStatus status) {
        if (this.status == status) {
            return;
        }

        this.status = status;
        this.modifyStatusDate = LocalDateTime.now();
    }
}
