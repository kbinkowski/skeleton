package com.kbware.skeleton.module.user.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserData {

    String id;

    String firstname;

    String lastname;

    String login;

    String email;

    UserStatus status;

    List<UserRole> roles;

    LocalDateTime activationDate;

    LocalDateTime deactivationDate;

    LocalDateTime lastLoginDate;

}
