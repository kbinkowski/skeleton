package com.kbware.skeleton.module.user.dto;

import com.kbware.skeleton.module.user.model.UserRole;
import com.kbware.skeleton.module.user.model.UserStatus;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SaveUserRequest {

    @NotBlank
    String firstname;

    @NotBlank
    String lastname;

    @NotBlank
    String login;

    @NotBlank
    String email;

    @NotNull
    UserStatus status;

    @NotEmpty
    List<UserRole> roles;

}
