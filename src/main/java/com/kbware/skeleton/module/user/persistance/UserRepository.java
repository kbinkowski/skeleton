package com.kbware.skeleton.module.user.persistance;

import com.kbware.skeleton.module.user.model.UserStatus;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<UserDocument, String>,
        QuerydslPredicateExecutor<UserDocument> {

    Optional<UserDocument> findByLoginAndStatus(String login, UserStatus status);

    Page<UserDocument> findAll(Predicate predicate, Pageable pageable);

}
