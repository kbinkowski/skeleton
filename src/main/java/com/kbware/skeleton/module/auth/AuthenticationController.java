package com.kbware.skeleton.module.auth;

import com.kbware.skeleton.module.auth.model.AuthenticationToken;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationProvider authenticationProvider;

    private final AuthenticationUserService userService;

    @PostMapping(value = "/login")
    public ResponseEntity<UserDetails> login(@Validated @RequestBody LoginRequest request) {

        AuthenticationToken authentication = (AuthenticationToken) authenticationProvider.authenticate(
                new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword())
        );

        if (authentication.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetails user = authentication.getPrincipal();
            userService.setLastLoginDate(user.getUsername());
            return ResponseEntity.ok(user);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping(value = "/refresh")
    public ResponseEntity<Void> refreshToken() {
        return ResponseEntity.ok().build();
    }

}
