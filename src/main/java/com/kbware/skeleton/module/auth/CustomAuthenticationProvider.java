package com.kbware.skeleton.module.auth;

import com.kbware.skeleton.module.auth.model.AuthenticationToken;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final UserDetailsService userService;

    private final JwtService jwtService;

    private final PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) {
        var token = (UsernamePasswordAuthenticationToken) authentication;
        if (token.getPrincipal() == null) {
            throw new BadCredentialsException("bad credentials");
        }

        String login = token.getPrincipal().toString();
        String password = token.getCredentials().toString();

        return Optional.of(userService.loadUserByUsername(login))
                .filter(user -> passwordEncoder.matches(password, user.getPassword()))
                .map(user -> authenticatedToken(user, authentication))
                .orElseThrow(() -> new BadCredentialsException("bad credentials"));
    }

    @Override
    public boolean supports(Class<? extends Object> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    private Authentication authenticatedToken(UserDetails user, Authentication original) {
        String jwt = jwtService.generateToken(
                user.getUsername(),
                user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())
        );

        var token = new AuthenticationToken(user, user.getAuthorities(), jwt);
        token.setDetails(original.getDetails());
        return token;
    }
}
