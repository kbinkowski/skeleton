package com.kbware.skeleton.module.auth;

import com.kbware.skeleton.module.auth.model.AuthenticationUser;
import com.kbware.skeleton.module.user.model.UserStatus;
import com.kbware.skeleton.module.user.persistance.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AuthenticationUserService implements UserDetailsService {

    private final UserRepository repository;

    private final AuthenticationUserMapper mapper;

    public AuthenticationUser loadUserByUsername(String login) {
        return repository.findByLoginAndStatus(login, UserStatus.ACTIVE)
                .map(mapper::toAuthenticationUser)
                .orElseThrow(() -> new UsernameNotFoundException("user not found: " + login));
    }

    public void setLastLoginDate(String login) {
        var user = loadUserByUsername(login);
        user.setLastLoginDate(LocalDateTime.now());
    }

}
