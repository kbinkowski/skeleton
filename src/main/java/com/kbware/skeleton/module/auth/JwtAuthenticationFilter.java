package com.kbware.skeleton.module.auth;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.kbware.skeleton.module.auth.model.AuthenticationToken;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private static final String TOKEN_PREFIX = "Bearer";
    private static final String CLAIM_ROLE = "role";

    private final UserDetailsService userService;

    private final JwtService jwtService;

    public JwtAuthenticationFilter(UserDetailsService userService, JwtService jwtService) {
        this.userService = userService;
        this.jwtService = jwtService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        AuthenticationToken authentication = getAuthentication(req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private AuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (token != null) {
            DecodedJWT jwt = jwtService.verifyToken(token.replace(TOKEN_PREFIX, "").trim());

            return Optional.of(userService.loadUserByUsername(jwt.getSubject()))
                    .map(user -> createToken(jwt, user))
                    .orElse(null);
        }
        return null;
    }

    private AuthenticationToken createToken(DecodedJWT jwt, UserDetails user) {
        String scope = jwt.getClaim(CLAIM_ROLE).asString();
        List<SimpleGrantedAuthority> roles = Collections.singletonList(new SimpleGrantedAuthority(scope));
        return new AuthenticationToken(user, roles, jwtService.refreshToken(jwt));
    }
}
