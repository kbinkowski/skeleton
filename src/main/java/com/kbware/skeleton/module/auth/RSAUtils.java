package com.kbware.skeleton.module.auth;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RSAUtils {

    public static KeyPair parseKeys(String publicKey, String privateKey)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        KeyFactory kf = KeyFactory.getInstance("RSA");

        RSAPublicKey rsaPublicKey = null;
        if (publicKey != null) {
            String publicKeyContent = publicKey
                    .replaceAll("\\n", "")
                    .replaceAll("\\r", "")
                    .replace("-----BEGIN PUBLIC KEY-----", "")
                    .replace("-----END PUBLIC KEY-----", "");

            X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
            rsaPublicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);
        }

        RSAPrivateKey rsaPrivateKey = null;
        if (privateKey != null) {
            String privateKeyContent = privateKey
                    .replaceAll("\\n", "")
                    .replaceAll("\\r", "")
                    .replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "");

            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
            rsaPrivateKey = (RSAPrivateKey) kf.generatePrivate(privateKeySpec);
        }

        return new KeyPair(rsaPublicKey, rsaPrivateKey);
    }

}
