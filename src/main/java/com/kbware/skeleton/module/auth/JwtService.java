package com.kbware.skeleton.module.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
public class JwtService {

    private static final String CLAIM_ROLE = "role";

    private final Algorithm algorithm;

    private final Integer expireTime;

    public String generateToken(String login, Collection<String> authorities) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, expireTime);
        JWTCreator.Builder jwtBuilder = JWT.create()
                .withExpiresAt(calendar.getTime())
                .withSubject(login);

        authorities.stream()
                .findFirst()
                .ifPresent(role -> jwtBuilder.withClaim(CLAIM_ROLE, role));

        return jwtBuilder.sign(algorithm);
    }

    public DecodedJWT verifyToken(String token) {
        try {
            JWTVerifier verifier = JWT.require(algorithm).build();
            return verifier.verify(token);
        } catch (JWTVerificationException ex) {
            throw new BadCredentialsException("invalid jwt", ex);
        }
    }

    public String refreshToken(DecodedJWT token) {
        return generateToken(token.getSubject(), List.of(token.getClaim(CLAIM_ROLE).asString()));
    }
}
