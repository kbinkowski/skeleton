package com.kbware.skeleton.module.auth.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@EqualsAndHashCode(callSuper = true)
public class AuthenticationToken extends AbstractAuthenticationToken {

    private UserDetails user;

    @Getter
    private String jwt;

    public AuthenticationToken(UserDetails user, Collection<? extends GrantedAuthority> auths, String jwt) {
        super(auths);
        setAuthenticated(true);
        this.user = user;
        this.jwt = jwt;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public UserDetails getPrincipal() {
        return user;
    }

}
