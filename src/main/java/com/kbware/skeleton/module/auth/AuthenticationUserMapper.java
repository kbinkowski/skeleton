package com.kbware.skeleton.module.auth;

import com.kbware.skeleton.module.auth.model.AuthenticationUser;
import com.kbware.skeleton.module.user.persistance.UserDocument;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AuthenticationUserMapper {

    AuthenticationUser toAuthenticationUser(UserDocument user);

}
