package com.kbware.skeleton.common.util;

import com.kbware.skeleton.module.auth.model.AuthenticationUser;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SecurityUtils {

    public static Optional<UserDetails> currentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
            Object principal = authentication.getPrincipal();
            if (principal == null) {
                return Optional.empty();
            } else if (principal instanceof UserDetails) {
                return Optional.of((UserDetails) principal);
            } else {
                throw new ClassCastException(
                        principal.getClass().getCanonicalName()
                                + " is not assignable to "
                                + AuthenticationUser.class.getCanonicalName()
                );
            }
        } else {
            return Optional.empty();
        }
    }

    public static String getTokenWithLogin(String token) {
        return SecurityUtils.currentUser().orElseThrow(IllegalStateException::new).getUsername() + token;
    }

}
