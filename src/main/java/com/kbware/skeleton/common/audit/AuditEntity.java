package com.kbware.skeleton.common.audit;

import lombok.AccessLevel;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.*;
import org.springframework.data.domain.Auditable;

import java.time.LocalDateTime;
import java.util.Optional;

@Setter
@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class AuditEntity implements Auditable<String, String, LocalDateTime> {

    @Id
    String id;
    @Version
    Long revision;

    @CreatedDate
    LocalDateTime createdDate;
    @CreatedBy
    String createdBy;
    @LastModifiedDate
    LocalDateTime lastModifiedDate;
    @LastModifiedBy
    String lastModifiedBy;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return id == null;
    }

    @Override
    public Optional<String> getCreatedBy() {
        return Optional.of(createdBy);
    }

    @Override
    public Optional<LocalDateTime> getCreatedDate() {
        return Optional.of(createdDate);
    }

    @Override
    public Optional<String> getLastModifiedBy() {
        return Optional.of(lastModifiedBy);
    }

    @Override
    public Optional<LocalDateTime> getLastModifiedDate() {
        return Optional.of(lastModifiedDate);
    }

}
