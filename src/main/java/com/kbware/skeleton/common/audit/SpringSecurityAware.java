package com.kbware.skeleton.common.audit;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

@RequiredArgsConstructor
public class SpringSecurityAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return getAuthenticationPrincipal()
                .map(UserDetails::getUsername);
    }

    private Optional<UserDetails> getAuthenticationPrincipal() {
        return Optional.of(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .filter(Authentication::isAuthenticated)
                .map(auth -> ((UserDetails) auth.getPrincipal()));
    }
}
