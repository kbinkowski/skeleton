package com.kbware.skeleton.common.select;

import lombok.NonNull;
import lombok.Value;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Value(staticConstructor = "of")
public class SelectEntry {

    @NonNull String id;

    @NonNull String value;

    public static SelectEntry toEntry(IsSelectable selectable) {
        return SelectEntry.of(selectable.getId(), selectable.getValue());
    }

    public static List<SelectEntry> toEntries(IsSelectable[] selectables) {
        return Arrays.stream(selectables).map(SelectEntry::toEntry)
                .collect(Collectors.toList());
    }


}
