package com.kbware.skeleton.common.select;

public interface IsSelectable {

    String getId();

    String getValue();

}
