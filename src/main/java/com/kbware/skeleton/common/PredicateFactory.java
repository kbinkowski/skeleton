package com.kbware.skeleton.common;

import com.querydsl.core.types.dsl.*;
import lombok.RequiredArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public abstract class PredicateFactory {

    protected final BooleanExpression anyBoolean;

    protected BooleanExpression createTermCriteria(String value, StringExpression... fields) {
        return value == null ? anyBoolean : Arrays.stream(fields)
                .map(f -> f.containsIgnoreCase(value))
                .reduce(BooleanExpression::or)
                .orElseThrow(IllegalArgumentException::new);
    }

    protected BooleanExpression createTimeCriteria(
            LocalDate from,
            LocalDate to,
            DateTimePath<LocalDateTime> dateField
    ) {
        if (from != null && to != null) {
            return dateField.between(from.atStartOfDay(), to.atStartOfDay().plusDays(1));
        } else if (from != null) {
            return dateField.after(from.atStartOfDay());
        } else if (to != null) {
            return dateField.before(to.atStartOfDay().plusDays(1));
        } else {
            return anyBoolean;
        }
    }

    protected <T> BooleanExpression createCriteriaEqual(T value, SimpleExpression<T> field) {
        return Optional.ofNullable(value)
                .map(field::eq)
                .orElse(anyBoolean);
    }

    protected <T> BooleanExpression createCriteriaIn(List<T> value, SimpleExpression<T> field) {
        return CollectionUtils.isEmpty(value) ? anyBoolean : field.in(value);
    }

    protected <T extends Enum<T>> BooleanExpression createCriteriaContains(T value, ListPath<T, EnumPath<T>> field) {
        return Optional.ofNullable(value)
                .map(field::contains)
                .orElse(anyBoolean);
    }
}
