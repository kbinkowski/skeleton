package com.kbware.skeleton.common.exception;

import com.auth0.jwt.exceptions.JWTVerificationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class CustomErrorHandler extends ResponseEntityExceptionHandler {

    private static final String RESOURCE_MODIFIED_ERROR = "Zasób został już zmodyfikowany";

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handle(NotFoundException e) {
        log.error("Not found: {}", e.getMessage());
    }

    @ExceptionHandler(PermissionDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handle(PermissionDeniedException e) {
        log.error("Permission exception: {}", e.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handle(AccessDeniedException e) {
        log.error("Access denied exception: {}", e.getMessage());
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handle(BadCredentialsException e) {
        log.error("Bad credential exception: {}", e.getMessage());
    }

    @ExceptionHandler(JWTVerificationException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handle(JWTVerificationException e) {
        log.error("JWT exception: {}", e.getMessage());
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handle(BusinessException e) {
        log.error("Business exception: {}", e.getMessage());
        return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(e.getMessage());
    }

    @ExceptionHandler(OptimisticLockingFailureException.class)
    public ResponseEntity<Object> handle(OptimisticLockingFailureException e) {
        log.error("OptimisticLockingFailure exception: {}", e.getMessage());
        return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body(RESOURCE_MODIFIED_ERROR);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public void handleException(Exception e) {
        log.error("unknown error", e);
    }

}
