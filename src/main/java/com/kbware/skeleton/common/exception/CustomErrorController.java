package com.kbware.skeleton.common.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
public class CustomErrorController extends AbstractErrorController {

    public CustomErrorController(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @Override
    public String getErrorPath() {
        return "${error.path:/error}";
    }

    @GetMapping("${error.path:/error}")
    public ResponseEntity<Void> errorJson(HttpServletRequest request) {
        HttpStatus statusCode = this.getStatus(request);
        if (statusCode.is4xxClientError()) {
            return ResponseEntity.status(statusCode).build();
        }
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE.value()).build();
    }

}
