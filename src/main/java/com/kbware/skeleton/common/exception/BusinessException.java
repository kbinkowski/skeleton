package com.kbware.skeleton.common.exception;

public class BusinessException extends RuntimeException {
    public BusinessException(String errorMessage, Object... parameters) {
        super(String.format(errorMessage, parameters));
    }
}
