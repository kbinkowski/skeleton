package com.kbware.skeleton.common.exception;

import lombok.NonNull;

public class NotFoundException extends RuntimeException {
    public NotFoundException(@NonNull String resource, String id) {
        super(String.format("%s with id=%s", resource, id));
    }
}
