package com.kbware.skeleton.config;

import com.kbware.skeleton.common.audit.SpringSecurityAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@Configuration
@EnableMongoAuditing
class AuditConfiguration {

    @Bean
    public SpringSecurityAware springSecurityAware() {
        return new SpringSecurityAware();
    }

}
