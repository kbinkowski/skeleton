package com.kbware.skeleton.config;

import com.auth0.jwt.algorithms.Algorithm;
import com.kbware.skeleton.module.auth.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final SecurityProperties properties;

    private final AuthenticationUserService authenticationUserService;

    private static final String[] SWAGGER_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui/**",
            "/v3/api-docs",
            "/webjars/**"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //@formatter:off
        http
                .formLogin().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                // permissions
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, SWAGGER_WHITELIST).permitAll()
                .antMatchers("/api/auth/login").permitAll()
                .antMatchers("/actuator/**").permitAll()
                .antMatchers("/error").permitAll()
                .anyRequest().fullyAuthenticated()
                // filters
                .and()
                .addFilterBefore(getAuthenticationFilter(), BasicAuthenticationFilter.class)
        ;
        // @formatter:on
    }

    @Bean
    AuthenticationProvider authenticationProvider() throws InvalidKeySpecException, NoSuchAlgorithmException {
        return new CustomAuthenticationProvider(authenticationUserService, jwtService(), passwordEncoder());
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JwtService jwtService() throws InvalidKeySpecException, NoSuchAlgorithmException {
        return new JwtService(getAlgorithm(), properties.getExpireTime());
    }

    private JwtAuthenticationFilter getAuthenticationFilter() throws InvalidKeySpecException, NoSuchAlgorithmException {
        return new JwtAuthenticationFilter(authenticationUserService, jwtService());
    }

    private Algorithm getAlgorithm() throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeyPair keyPair = RSAUtils.parseKeys(properties.getPublicKey(), properties.getPrivateKey());
        return Algorithm.RSA256((RSAPublicKey) keyPair.getPublic(), (RSAPrivateKey) keyPair.getPrivate());
    }

}
