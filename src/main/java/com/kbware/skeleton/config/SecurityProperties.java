package com.kbware.skeleton.config;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("security.jwt")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SecurityProperties {

    String publicKey;

    String privateKey;

    Integer expireTime;

}
